//! Define `MockableLibc` and implement it for `RealLibc`
//!
//! The output looks rather like this:
//!
//! ```text
//! trait MockableLibc: Copy + Deref<Target=MockableLibcFunctions> {}
//!
//! type LibcFn_getpwnam_r = unsafe extern "C" fn(...) -> c_int;
//!
//! struct MockableLibcFunctions {
//!     getpwnam_r: LibcFn_getpwnam_r,
//!    ..
//! }
//!
//! impl Deref for MockableLibc { ...
//!     ...
//!        static M = MockableLibcFunctions {
//!            getpwnam_r: libc::getpwnam_r,
//!        }
//! ```
//!
//! ### SAFETY
//!
//! This approach guarantees that the type and value of `mlibc.getpwnam_r`
//! are precisely those of `libc::getpwnam_r`.
//!
//! This is very important for the safety and correctness
//! of the implementation,
//! and also ensures that the tests with mocked libc are faithful.
//!
//! ### Alternative approaches
//!
//! We could have just had `getpwnam_inner` take
//! `mlibc: &'static MockableLibcFunctions`
//! but that might well result in a reified pointer.
//!
//! The present approach means the call site is monomorphised,
//! and hopefully the Deref and constant returned value are inlined
//! and vanish.

use super::*;

#[derive(Copy, Clone)]
pub(crate) struct RealLibc;

#[allow(non_camel_case_types)]
pub(crate) type LibcFn_sysconf = unsafe extern "C" fn(c_int) -> c_long;

#[allow(non_camel_case_types)]
pub(crate) type LibcFn_getresid<I> =
    unsafe extern "C" fn(ruid: *mut I, euid: *mut I, suid: *mut I) -> c_int;

#[allow(non_camel_case_types)]
pub(crate) type LibcFn_getresuid = LibcFn_getresid<uid_t>;
#[allow(non_camel_case_types)]
pub(crate) type LibcFn_getresgid = LibcFn_getresid<gid_t>;

#[allow(non_camel_case_types)]
pub(crate) type LibcFn_getgroups =
    unsafe extern "C" fn(ngroups_max: c_int, groups: *mut gid_t) -> c_int;

macro_rules! define_mockable { {
    $(
        ($function:ident $(, $passwd:ident, $key:ty )? ),
    )*
} => { paste!{
    $(
        $(
        #[allow(non_camel_case_types)]
        pub(crate) type [< LibcFn_ $function >] = unsafe extern "C" fn(
            $key,
            *mut libc::$passwd,
            *mut c_char,
            size_t,
            *mut *mut libc::$passwd,
        ) -> c_int;
        )?
    )*

    #[derive(Adhoc)]
    pub(crate) struct MockableLibcFunctions {
        $(
            pub(crate) $function: [< LibcFn_ $function >],
        )*
    }

    pub(crate) trait MockableLibc: Copy
        + Deref<Target=MockableLibcFunctions> {}

    impl Deref for RealLibc {
        type Target = MockableLibcFunctions;
        fn deref(&self) -> &MockableLibcFunctions {
            static M: MockableLibcFunctions = MockableLibcFunctions {
                $(
                    $function: libc::$function,
                )*
            };
            &M
        }
    }

    impl MockableLibc for RealLibc {}
} } }

define_mockable! {
    (sysconf),
    (getresuid),
    (getresgid),
    (getgroups),
    (getpwnam_r, passwd, *const c_char),
    (getpwuid_r, passwd, uid_t),
    (getgrnam_r, group, *const c_char),
    (getgrgid_r, group, gid_t),
}
